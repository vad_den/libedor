$(document).ready(function(){

setTimeout(function() {
 if ($('#country-select').length) {
   $.magnificPopup.open({
    items: {
        src: '#country-select' 
    },
	callbacks: {
        open: function() {
            $('.country-list').slick({
			  infinite: true,
			  slidesToShow: 4,
			  slidesToScroll: 4,
			  arrows:true,
              nextArrow: '<img class="right" src="image/right-arrow.png">',
              prevArrow: '<img class="left" src="image/left-arrow.png">',
			  dots:false,
			});
        }
    },
    type: 'inline'
      });
   }
 }, 1000);


$('.open-popup-link').magnificPopup({
  type:'inline',
  midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
});

	(function($) {
		$(function() {
			$('input').styler({
				selectSearch: true
			});
		});
		})(jQuery);
	
        $('.slider-top').slick({
            аccessibility:true,
            autoplay:true,
            arrows:true,
			fade: true,
            nextArrow: '<img class="right" src="image/right-arrow.png">',
            prevArrow: '<img class="left" src="image/left-arrow.png">',
            slidesToShow: 1,
            slidesToScroll: 1,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        infinite: true,
                        dots: false
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ]
        });

        $('.slider-sert').slick({
            infinite: true,
            аccessibility:true,
            autoplay:true,
            arrows:true,
            nextArrow: '<span><img class="right" src="image/right-arrow.png"></span>',
            prevArrow: '<span><img class="left" src="image/left-arrow.png"></span>',
            slidesToShow: 4,
            slidesToScroll: 1,
            });

        $('.slider-reviews').slick({
            infinite: true,
            аccessibility:true,
            autoplay:true,            
            slidesToShow: 2,
            slidesToScroll: 2,
            dots: true,
        });
        $( ".menu-btn" ).click(function() {
            $('.menu').toggleClass('open');
            $(this).toggleClass('is-active');
        });
        $("#menu"+"#app").on("click","a", function (event) {
            event.preventDefault();
            var id  = $(this).attr('href'),
                top = $(id).offset().top;
            $('body,html').animate({scrollTop: top}, 1500);
        });

});
// JavaScript Document
 
$(window).scroll(function() {    
    var scroll = $(window).scrollTop();
    if (scroll >= 100) {
        $("header").addClass("fixed");
		$(".con").slideUp();
    } else {
        $("header").removeClass("fixed");
		$(".con").slideDown();
    }
});

//arrow slider
$(window).scroll(function(){
    $(".arrow-slider").css("opacity", 1 - $(window).scrollTo() / 250);
    //250 is fade pixels
});

//
$(document).ready(function(){
    $("#arrow-slider").on("click","a", function (event) {
        event.preventDefault();
        var id  = $(this).attr('href'),
            top = $(id).offset().top;
        $('body,html').animate({scrollTop: top}, 1500);
    });
});

//slider sert

$('.slider-sert').magnificPopup({
        delegate: 'a', // the selector for gallery item
        type: 'image',
        gallery: {
            enabled:true
        }

});


 $( ".faq .container" ).each(function(index) {
        $(this).on("click", function(){
 $(this).next('.content').toggle();
 $(this).toggleClass('open');
        });
 });